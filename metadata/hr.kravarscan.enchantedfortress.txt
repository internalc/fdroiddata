Categories:Games
License:LGPL-3.0
Web Site:
Source Code:https://github.com/subchannel13/EnchantedFortress
Issue Tracker:https://github.com/subchannel13/EnchantedFortress/issues
Donate:https://www.paypal.me/IvanKravarscan/5

Auto Name:Enchanted Fortress
Summary:Build, manage and defend your castle
Description:
A simple game inspired by Age of Castles. Distribute your population to farming,
construction, guard duty and scholarship, survive demon invasion and find the
way to banish them forever.
.

Repo Type:git
Repo:https://github.com/subchannel13/EnchantedFortress

Build:1.0,1
    commit=0db12acb24f11fbb892f8c1e43e769254adadb4c
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1
